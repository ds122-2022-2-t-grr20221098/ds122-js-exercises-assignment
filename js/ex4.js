// Crie uma função que inverte os elemento de um array. Essa função deve ser do
// tipo in-place, ou seja, deve realizar a inversão sem a necessidade de um
// array auxiliar. Por exemplo:

let array = [1, 2, 3, 4, 5];
let aux;

function inverteArray(array) {
  let i = 0;
  let j = array.length - 1;
  while (i < j) {
    aux = array[i];
    array[i] = array[j];
    array[j] = aux;
    i++;
    j--;
  }
  return array;
}
console.log(inverteArray(array));
