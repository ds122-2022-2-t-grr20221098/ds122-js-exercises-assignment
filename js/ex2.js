// Escreva um programa que cria uma string que representa um quadro 8x8, utilizando
// o caractere de nova linha "\n" para separar cada uma das linhas. Por exemplo:
// "linha1..\nlinha2..". Cada posição do quadro deve ser representada por um espaço
// em branco (" ") ou um "#". Os caracteres devem produzir a forma de um tabuleiro
// de xadrez.
// Ao passar a string produzida ao comando console.log, o programa deve exibir

let linha1 = [" # # # #"];
let linha2 = ["# # # # "];
let espaçoembranco = " ";
let i;

for (i = 1; i < 9; ++i) {
  if (i % 2 === 1) {
    espaçoembranco += linha1;
  } else {
    espaçoembranco += linha2;
  }

  espaçoembranco += "\n";
}

console.log(espaçoembranco);
console.log("\n");
