// Crie uma função que retorna a soma dos elementos de um array. Por exemplo:

// var a = [1,2,3];

// console.log(soma(a));
// // -> 6

function soma(arr) {
  let acumulador = 0;

  for (let i = 0; i < arr.length; i++) {
    acumulador += arr[i];
  }

  return acumulador;
}

const array = [1, 2, 3, 4, 5, 6, 7, 8, 9];
const resultado = soma(array);
console.log(resultado);
