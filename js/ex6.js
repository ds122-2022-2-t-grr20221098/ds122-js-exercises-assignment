// Crie uma função que procura por uma substring dentro um uma string. Caso o trecho
// procurado exista na string, a função retorna a posição em que o trecho começa.
// Caso contrário, a função deve retornar o valor -1.

let string = "maria";
let letra = "a";

function procuraSubstring(string) {
  let i;
  let contador = 0;
  for (i = 0; i < string.length; ++i) {
    if (letra === string[i]) {
      contador++;
    }
  }
  return contador;
}
console.log(procuraSubstring("a", "maria"));
