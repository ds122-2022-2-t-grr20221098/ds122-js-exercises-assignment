// Crie uma função que conta a quantidade de aparições de uma dada letra em uma string.

let letra;
function contaLetra(letra, string) {
  let i;
  let contador = 0;
  for (i = 0; i < string.length; ++i) {
    if (letra === string[i]) {
      contador++;
    }
  }
  return contador;
}

console.log(contaLetra("a", "maria"));

// ++i / i = i + 1;
// i++ / i = i + 1;
